#ifndef four_batched_multiple_kernels_cu
#define four_batched_multiple_kernels_cu
// =======================================================================
// 4 randomized cgesv batched computation using multiple kernels
//
// Modifications
//    Chien  21-05-05:   Originally Created
//
// =======================================================================
#include <stdio.h>
#include <stdlib.h>
#include <cstdio>
#include <iostream>
#include <iomanip>
#include <cstring>

// -- cuda included --
#include <cuda.h>
#include <cuda_runtime.h>

// -- magma --
#include "flops.h"
#include "magma_v2.h"
#include "magma_lapack.h"
#include "magma_internal.h"
#undef max
#undef min
#include "magma_templates.h"
#include "sync.cuh"
#undef max
#undef min
#include "shuffle.cuh"
#undef max
#undef min
#include "batched_kernel_param.h"

// -- project headers --
#include "magmaHC-methods.cuh"
#include "magmaHC-kernels.h"

namespace magmaHCWrapper {

  void four_batched_multiple_kernels(int batchSize, int matrixSize) {
    magma_init();
    magma_print_environment();

    real_Double_t   gflops, cpu_perf, cpu_time, gpu_perf, gpu_time;
    real_Double_t   gpu_time_first_solve, gpu_time_second_solve, gpu_time_third_solve, gpu_time_fourth_solve;
    real_Double_t   gpu_time_new_data_1, gpu_time_new_data_2, gpu_time_new_data_3;
    float           *work;
    magmaFloatComplex c_one     = MAGMA_C_ONE;
    magmaFloatComplex c_neg_one = MAGMA_C_NEG_ONE;
    magmaFloatComplex *h_A, *h_B, *h_X_gpu, *h_X_cpu;
    magmaFloatComplex *h_one_mat, *h_one_vec;
    magmaFloatComplex *h_verifyA;
    magmaFloatComplex_ptr d_A, d_B;
    magma_int_t *dipiv, *dinfo_array;
    magma_int_t *ipiv, *cpu_info;
    magma_int_t N, nrhs, lda, ldb, ldda, lddb, sizeA, sizeB;
    magma_int_t ione = 1;

    magma_int_t ISEED[4] = {0,0,0,1};
    //int status = 0;
    nrhs = 1;

    magma_int_t batchCount;
    magmaFloatComplex **dA_array = NULL;
    magmaFloatComplex **dB_array = NULL;
    magma_int_t     **dipiv_array = NULL;

    bool use_lapack = 1;
    N = matrixSize;
    batchCount = batchSize;

    magma_queue_t my_queue;    // magma queue variable, internally holds a cuda stream and a cublas handle
    magma_device_t cdev;       // variable to indicate current gpu id

    magma_getdevice( &cdev );
    magma_queue_create( cdev, &my_queue );     // create a queue on this cdev

    lda    = N;
    ldb    = lda;
    ldda   = magma_roundup( N, 32 );  // multiple of 32 by default
    lddb   = ldda;
    gflops = ( FLOPS_DGETRF( N, N ) + FLOPS_DGETRS( N, nrhs ) ) * batchCount / 1e9;

    sizeA = lda*N*batchCount;
    sizeB = ldb*nrhs*batchCount;

    magma_cmalloc_cpu( &h_A, sizeA );
    magma_cmalloc_cpu( &h_B, sizeB );
    magma_cmalloc_cpu( &h_X_gpu, sizeB );
    magma_cmalloc_cpu( &h_X_cpu, sizeB );
    magma_cmalloc_cpu( &h_one_mat, lda*N );
    magma_cmalloc_cpu( &h_one_vec, ldb*nrhs );
    magma_cmalloc_cpu( &h_verifyA, sizeA );
    //magma_cmalloc_cpu( &h_verifyB, sizeB );
    magma_smalloc_cpu( &work, N );
    magma_imalloc_cpu( &ipiv, batchCount*N );
    magma_imalloc_cpu( &cpu_info, batchCount );

    magma_cmalloc( &d_A, ldda*N*batchCount    );
    magma_cmalloc( &d_B, lddb*nrhs*batchCount );
    magma_imalloc( &dipiv, N * batchCount );
    magma_imalloc( &dinfo_array, batchCount );

    magma_malloc( (void**) &dA_array,    batchCount * sizeof(magmaFloatComplex*) );
    magma_malloc( (void**) &dB_array,    batchCount * sizeof(magmaFloatComplex*) );
    magma_malloc( (void**) &dipiv_array, batchCount * sizeof(magma_int_t*) );

    // -- Initialize the matrices --
    lapackf77_clarnv( &ione, ISEED, &sizeA, h_A );
    lapackf77_clarnv( &ione, ISEED, &sizeB, h_B );

    magma_csetmatrix( N, N*batchCount,    h_A, lda, d_A, ldda, my_queue );
    magma_csetmatrix( N, nrhs*batchCount, h_B, ldb, d_B, lddb, my_queue );

    magma_cset_pointer( dA_array, d_A, ldda, 0, 0, ldda*N, batchCount, my_queue );
    magma_cset_pointer( dB_array, d_B, lddb, 0, 0, lddb*nrhs, batchCount, my_queue );
    magma_iset_pointer( dipiv_array, dipiv, 1, 0, 0, N, batchCount, my_queue );

    //std::cout<<"Original:"<<std::endl;
    //magma_cprint(N, N, h_A, lda);
    //magma_cprint(N, nrhs, h_B, ldb);

    // ===================================================================
    // magma GPU cgesv batched solver
    // ===================================================================
    // -- multiple kernels --
    // -- 1st solve --
    std::cout<<"GPU computing ..."<<std::endl;
    gpu_time_first_solve = magma_sync_wtime( my_queue );
    kernel_cgesv_batched_four_rand_mulk(N, batchCount, my_queue, dA_array, ldda, dipiv_array, dB_array, lddb, dinfo_array);
    gpu_time_first_solve = magma_sync_wtime( my_queue ) - gpu_time_first_solve;
    check_correctness(batchCount, dinfo_array, cpu_info, my_queue);

    // -- 1st get new data --
    gpu_time_new_data_1 = magma_sync_wtime( my_queue );
    kernel_get_new_data_four_rand(N, batchCount, my_queue, dA_array, ldda, dB_array);
    gpu_time_new_data_1 = magma_sync_wtime( my_queue ) - gpu_time_new_data_1;

    // -- 2nd solve --
    gpu_time_second_solve = magma_sync_wtime( my_queue );
    kernel_cgesv_batched_four_rand_mulk(N, batchCount, my_queue, dA_array, ldda, dipiv_array, dB_array, lddb, dinfo_array);
    gpu_time_second_solve = magma_sync_wtime( my_queue ) - gpu_time_second_solve;
    check_correctness(batchCount, dinfo_array, cpu_info, my_queue);

    // -- 2nd get new data --
    gpu_time_new_data_2 = magma_sync_wtime( my_queue );
    kernel_get_new_data_four_rand(N, batchCount, my_queue, dA_array, ldda, dB_array);
    gpu_time_new_data_2 = magma_sync_wtime( my_queue ) - gpu_time_new_data_2;

    // -- 3rd solve --
    gpu_time_third_solve = magma_sync_wtime( my_queue );
    kernel_cgesv_batched_four_rand_mulk(N, batchCount, my_queue, dA_array, ldda, dipiv_array, dB_array, lddb, dinfo_array);
    gpu_time_third_solve = magma_sync_wtime( my_queue ) - gpu_time_third_solve;
    check_correctness(batchCount, dinfo_array, cpu_info, my_queue);

    // -- 3rd get new data --
    gpu_time_new_data_3 = magma_sync_wtime( my_queue );
    kernel_get_new_data_four_rand(N, batchCount, my_queue, dA_array, ldda, dB_array);
    gpu_time_new_data_3 = magma_sync_wtime( my_queue ) - gpu_time_new_data_3;

    // -- 4th solve --
    gpu_time_fourth_solve = magma_sync_wtime( my_queue );
    kernel_cgesv_batched_four_rand_mulk(N, batchCount, my_queue, dA_array, ldda, dipiv_array, dB_array, lddb, dinfo_array);
    gpu_time_fourth_solve = magma_sync_wtime( my_queue ) - gpu_time_fourth_solve;
    check_correctness(batchCount, dinfo_array, cpu_info, my_queue);

    // -- check returns from the kernel --
    //magma_cgetmatrix( N, N*batchCount, d_A, ldda, h_verifyA, lda, my_queue );
    magma_cgetmatrix( N, nrhs*batchCount, d_B, lddb, h_X_gpu, ldb, my_queue );
    //magma_cprint(N, N, h_verifyA, lda);
    //magma_cprint(N, nrhs, h_X_gpu + 3 * ldb * nrhs, ldb);

    // -- compute gpu time --
    gpu_time = gpu_time_first_solve + gpu_time_second_solve + gpu_time_third_solve + gpu_time_fourth_solve;
    gpu_time += gpu_time_new_data_1 + gpu_time_new_data_2 + gpu_time_new_data_3;
    gpu_perf = gflops / gpu_time;

    //=====================================================================
    // CPU cgesv batched solver
    //=====================================================================
    if (use_lapack) {
      std::cout<<"CPU computing ..."<<std::endl;
      for(int i = 0; i < N; i++) {
        for(int j = 0; j < N; j++) {
          h_one_mat[i*N+j] = c_one;
        }
        h_one_vec[i] = c_one;
      }
      cpu_time = magma_wtime();
      CPU_four_rand(h_X_cpu, h_A, h_B, h_one_mat, h_one_vec, batchCount, N, nrhs, ipiv, lda, ldb);
      cpu_time = magma_wtime() - cpu_time;
      cpu_perf = gflops / cpu_time;

      //=====================================================================
      // Compute Residual
      //=====================================================================
      bool okay = compute_residual(h_X_cpu, h_X_gpu, N, nrhs, batchCount, ldb, work);

      // -- print out the timing results --
      printf("%% BatchCount   N  NRHS   CPU Gflop/s (msec)   GPU Gflop/s (msec)   Residual\n");
      printf("%%=============================================================================\n");
      printf( "%10lld %5lld %5lld    %7.2f (%7.2f)    %7.2f (%7.2f)      %s\n",
                (long long) batchCount, (long long) N, (long long) nrhs,
                cpu_perf, cpu_time*1000, gpu_perf, gpu_time*1000, (okay ? "ok" : "failed"));
    }
    else {
      printf("%% BatchCount   N  NRHS   GPU Gflop/s (msec)\n");
      printf("%%=============================================\n");
      printf( "%10lld %5lld %5lld    %7.2f (%7.2f)\n",
                (long long) batchCount, (long long) N, (long long) nrhs, gpu_perf, gpu_time*1000);
    }

    magma_queue_destroy( my_queue );
    magma_free_cpu( h_A );
    magma_free_cpu( h_B );
    magma_free_cpu( h_X_gpu );
    magma_free_cpu( h_X_cpu );
    magma_free_cpu( h_one_mat );
    magma_free_cpu( h_one_vec );
    magma_free_cpu( h_verifyA );
    //magma_free_cpu( h_verifyB );
    magma_free_cpu( work );
    magma_free_cpu( ipiv );
    magma_free_cpu( cpu_info );

    magma_free( d_A );
    magma_free( d_B );
    magma_free( dipiv );
    magma_free( dinfo_array );
    magma_free( dA_array );
    magma_free( dB_array );
    magma_free( dipiv_array );
    fflush( stdout );
    printf( "\n" );
    magma_finalize();
  }

  void check_correctness(
    magma_int_t batchCount, magma_int_t *dinfo_array,
    magma_int_t *cpu_info, magma_queue_t my_queue)
  {
    // -- check correctness of results throught "dinfo_magma" and correctness of argument throught "info" --
    magma_getvector( batchCount, sizeof(magma_int_t), dinfo_array, 1, cpu_info, 1, my_queue );
    for (int i=0; i < batchCount; i++)
    {
        if (cpu_info[i] != 0 ) {
            printf("magma_dgesv_batched matrix %lld returned internal error %lld\n",
                    (long long) i, (long long) cpu_info[i] );
        }
    }
  }
} // end of namespace

#endif
