#ifndef input_info_cpp_
#define input_info_cpp_
// ============================================================================
// Specifications of input-info.h
//
// Modifications
//    Chien  21-05-02:   Originally created
//
// ============================================================================
#include <stdio.h>
#include <iostream>
#include <fstream>
#include <iomanip>
#include "input-info.h"

namespace cmdInputs {
  // -- print usage --
  void print_usage()
  {
    std::cerr << "===============================================================================================================\n";
    std::cerr << "Usage: ./magmaHC-main <input-argument> <command>\n\n";
    std::cerr << "<input-argument> <command>\n"
                 "       -t         <method>   # (or --test_method)  : test algorithm, <method> mist be either fb (four batched"
                 "                                                     linear systems) or rk (runge-kutta)\n"
                 "       -b         <value>    # (or --batchSize)    : batch size of linear systems Ax=b, specified by <value>\n"
                 "       -n         <value>    # (or --matrixSize)   : matrix size of A, specified by <value>\n"
                 "       -f         <method>   # (or --fuse)         : using a fused kernel to solve batched Ax=b, <method> can\n"
                 "                             #                       be either gm (global memory) or reg (registers)\n"
                 "       -m                    # (or --mulipltKernel): using multiple kernels to solve batched Ax=b\n"
                 "       -h                    # (or --help)         : print this help message\n\n";
    std::cerr << "----------------------- NOTICE -----------------------\n";
    std::cerr << "1. Choose either -f or -m, but not both.\n";
    std::cerr << "2. Order matters. Either -f or -m has to be the last argument.\n";
    std::cerr << "3. If not specified, the help message will be automatically shown.\n\n";
    std::cerr << "----------------------- Examples -----------------------\n";
    std::cerr << "./magmaHC-main -t fb -b 1024 -n 6 -f reg   # solve randomized four batched Ax=b problem using registers in the \n";
    std::cerr << "                                             fused kernel, where matrix A is 6x6, and the batch size is 1024.\n";
    std::cerr << "./magmaHC-main -t rk -b 2048 -n 18 -m      # solve randomized Runge-Kutta problem using multiple kernels, \n";
    std::cerr << "                                             where matrix A is 18x18, and the batch size is 2048.\n";
    std::cerr << "===============================================================================================================\n";
  }
}

#endif // input_info_cpp_
