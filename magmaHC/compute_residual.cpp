#ifndef compute_residual_cpp
#define compute_residual_cpp
// ============================================================================
// CPU computation for 4 randomized cgesv computation, looping over batch size
//
// Modifications
//    Chien  21-05-04:   Originally created
//
// ============================================================================
#include <stdio.h>
#include <stdlib.h>
#include <cstdio>
#include <iostream>
#include <iomanip>
#include <cstring>

// -- magma --
#include "magma_v2.h"
#include "magma_lapack.h"

// -- header included --
#include "magmaHC-kernels.h"

namespace magmaHCWrapper {

  extern "C" void
  CPU_four_rand(
    magmaFloatComplex *h_X_cpu,
    magmaFloatComplex *h_A, magmaFloatComplex *h_B,
    magmaFloatComplex *h_one_mat, magmaFloatComplex *h_one_vec,
    magma_int_t batchCount, magma_int_t N, magma_int_t nrhs,
    magma_int_t *ipiv, magma_int_t lda, magma_int_t ldb)
  {
    magma_int_t locinfo;
    magma_int_t inc = 1;
    magmaFloatComplex c_one = MAGMA_C_ONE;

    // -- compute on CPU --
    for (magma_int_t s=0; s < batchCount; s++)
    {
      // -- 1-1) 1st solve --
      lapackf77_cgesv( &N, &nrhs, h_A + s * lda * N, &lda, ipiv + s * N, h_B + s * ldb * nrhs, &ldb, &locinfo );
      if (locinfo != 0) {
         printf("lapackf77_cgesv matrix %lld returned error %lld: %s.\n",
                 (long long) s, (long long) locinfo, magma_strerror( locinfo ));
      }

      // -- 1-2) create new A and b --
      for(int n = 0; n < N; n++)
        blasf77_caxpy( &N, &c_one, h_one_mat, &inc, h_A + n*N + s * lda * N, &inc);
      blasf77_caxpy( &N, &c_one, h_one_vec, &inc, h_B + s * ldb * nrhs, &inc);

      // -- 2-1) 2nd solve --
      lapackf77_cgesv( &N, &nrhs, h_A + s * lda * N, &lda, ipiv + s * N, h_B + s * ldb * nrhs, &ldb, &locinfo );
      if (locinfo != 0) {
         printf("lapackf77_cgesv matrix %lld returned error %lld: %s.\n",
                 (long long) s, (long long) locinfo, magma_strerror( locinfo ));
      }

      // -- 2-2) create new A and b --
      for(int n = 0; n < N; n++)
        blasf77_caxpy( &N, &c_one, h_one_mat, &inc, h_A + n*N + s * lda * N, &inc);
      blasf77_caxpy( &N, &c_one, h_one_vec, &inc, h_B + s * ldb * nrhs, &inc);

      // -- 3-1) 3rd solve --
      lapackf77_cgesv( &N, &nrhs, h_A + s * lda * N, &lda, ipiv + s * N, h_B + s * ldb * nrhs, &ldb, &locinfo );
      if (locinfo != 0) {
         printf("lapackf77_cgesv matrix %lld returned error %lld: %s.\n",
                 (long long) s, (long long) locinfo, magma_strerror( locinfo ));
      }

      // -- 3-2) create new A and b --
      for(int n = 0; n < N; n++)
        blasf77_caxpy( &N, &c_one, h_one_mat, &inc, h_A + n*N + s * lda * N, &inc);
      blasf77_caxpy( &N, &c_one, h_one_vec, &inc, h_B + s * ldb * nrhs, &inc);

      // -- 4-1) 4th solve --
      lapackf77_cgesv( &N, &nrhs, h_A + s * lda * N, &lda, ipiv + s * N, h_B + s * ldb * nrhs, &ldb, &locinfo );
      if (locinfo != 0) {
         printf("lapackf77_cgesv matrix %lld returned error %lld: %s.\n",
                 (long long) s, (long long) locinfo, magma_strerror( locinfo ));
      }

      blasf77_ccopy( &N, h_B + s * ldb * nrhs, &inc, h_X_cpu + s * ldb * nrhs, &inc );
    }
  }

  extern "C" void
  CPU_runge_kutta_rand(
    magmaFloatComplex *h_A, magmaFloatComplex *h_B,
    magmaFloatComplex *h_S, magmaFloatComplex *h_S_cpu,
    magmaFloatComplex *h_one_mat, magmaFloatComplex *h_one_vec,
    magma_int_t batchCount, magma_int_t N, magma_int_t nrhs,
    magma_int_t *ipiv, magma_int_t lda, magma_int_t ldb)
  {
    magma_int_t locinfo;
    magma_int_t inc = 1;
    magmaFloatComplex c_one = MAGMA_C_ONE;
    magmaFloatComplex c_oneThird = MAGMA_C_MAKE(1.0/3.0, 0.0);
    magmaFloatComplex c_oneSixth = MAGMA_C_MAKE(1.0/6.0, 0.0);

    // -- compute on CPU --
    for (magma_int_t s=0; s < batchCount; s++)
    {
      blasf77_ccopy( &N, h_S + s * ldb * nrhs, &inc, h_S_cpu + s * ldb * nrhs, &inc );

      // -- 1-1) 1st solve --
      lapackf77_cgesv( &N, &nrhs, h_A + s * lda * N, &lda, ipiv + s * N, h_B + s * ldb * nrhs, &ldb, &locinfo );
      if (locinfo != 0) {
         printf("lapackf77_cgesv matrix %lld returned error %lld: %s.\n",
                 (long long) s, (long long) locinfo, magma_strerror( locinfo ));
      }

      // -- compute k1*1./6. --
      blasf77_caxpy( &N, &c_oneSixth, h_B + s * ldb * nrhs, &inc, h_S_cpu + s * ldb * nrhs, &inc);

      /*if(s == 0) {
        std::cout<<"k1*1./6."<<std::endl;
        magma_cprint(N, nrhs, h_S_cpu + s * ldb * nrhs, ldb);
      }*/

      // -- 1-2) create new A and b --
      for(int n = 0; n < N; n++)
        blasf77_caxpy( &N, &c_one, h_one_mat, &inc, h_A + n*N + s * lda * N, &inc);
      blasf77_caxpy( &N, &c_one, h_one_vec, &inc, h_B + s * ldb * nrhs, &inc);

      // -- 2-1) 2nd solve (k2) --
      lapackf77_cgesv( &N, &nrhs, h_A + s * lda * N, &lda, ipiv + s * N, h_B + s * ldb * nrhs, &ldb, &locinfo );
      if (locinfo != 0) {
         printf("lapackf77_cgesv matrix %lld returned error %lld: %s.\n",
                 (long long) s, (long long) locinfo, magma_strerror( locinfo ));
      }

      // -- compute k1*1./6. + k2*1./3. --
      blasf77_caxpy( &N, &c_oneThird, h_B + s * ldb * nrhs, &inc, h_S_cpu + s * ldb * nrhs, &inc);

      /*if(s == 0) {
        std::cout<<"k1*1./6. + k2*1./3."<<std::endl;
        magma_cprint(N, nrhs, h_S_cpu + s * ldb * nrhs, ldb);
      }*/

      // -- 2-2) create new A and b --
      for(int n = 0; n < N; n++)
        blasf77_caxpy( &N, &c_one, h_one_mat, &inc, h_A + n*N + s * lda * N, &inc);
      blasf77_caxpy( &N, &c_one, h_one_vec, &inc, h_B + s * ldb * nrhs, &inc);

      // -- 3-1) 3rd solve (k3) --
      lapackf77_cgesv( &N, &nrhs, h_A + s * lda * N, &lda, ipiv + s * N, h_B + s * ldb * nrhs, &ldb, &locinfo );
      if (locinfo != 0) {
         printf("lapackf77_cgesv matrix %lld returned error %lld: %s.\n",
                 (long long) s, (long long) locinfo, magma_strerror( locinfo ));
      }

      // -- compute k1*1./6. + k2*1./3. + k3*1./3. --
      blasf77_caxpy( &N, &c_oneThird, h_B + s * ldb * nrhs, &inc, h_S_cpu + s * ldb * nrhs, &inc);

      // -- 3-2) create new A and b --
      for(int n = 0; n < N; n++)
        blasf77_caxpy( &N, &c_one, h_one_mat, &inc, h_A + n*N + s * lda * N, &inc);
      blasf77_caxpy( &N, &c_one, h_one_vec, &inc, h_B + s * ldb * nrhs, &inc);

      // -- 4-1) 4th solve --
      lapackf77_cgesv( &N, &nrhs, h_A + s * lda * N, &lda, ipiv + s * N, h_B + s * ldb * nrhs, &ldb, &locinfo );
      if (locinfo != 0) {
         printf("lapackf77_cgesv matrix %lld returned error %lld: %s.\n",
                 (long long) s, (long long) locinfo, magma_strerror( locinfo ));
      }

      // -- compute k1*1./6. + k2*1./3. + k3*1./3. + k4*1./6. --
      blasf77_caxpy( &N, &c_oneSixth, h_B + s * ldb * nrhs, &inc, h_S_cpu + s * ldb * nrhs, &inc);

      /*if(s == 0) {
        std::cout<<"k1*1./6. + k2*1./3. + k3*1./3. + k4*1./6."<<std::endl;
        magma_cprint(N, nrhs, h_S_cpu + s * ldb * nrhs, ldb);
      }*/
    }
  }

  extern "C" bool compute_residual(
    magmaFloatComplex *h_X_cpu, magmaFloatComplex *h_X_gpu,
    magma_int_t N, magma_int_t nrhs, magma_int_t batchCount,
    magma_int_t ldb, float *work)
  {
    float err, error = 0;
    int idx;
    bool okay = 0;
    double tol = 1;
    magmaFloatComplex c_neg_one = MAGMA_C_NEG_ONE;
    magma_int_t inc = 1;

    // -- measure ||A-B||, norm distance between CPU and GPU --
    for (magma_int_t s=0; s < batchCount; s++)
    {
      /*if(s == 9) {
        magma_cprint(N, nrhs, h_X_gpu + s * ldb * nrhs, ldb);
        magma_cprint(N, nrhs, h_X_cpu + s * ldb * nrhs, ldb);
      }*/
      blasf77_caxpy( &N, &c_neg_one, h_X_cpu + s * ldb * nrhs, &inc, h_X_gpu + s * ldb * nrhs, &inc);

      /*if(s == 9) {
        magma_cprint(N, nrhs, h_X_gpu + s * ldb * nrhs, ldb);
      }*/

      // -- calculate infinite norms ("I") of matrices --
      err = lapackf77_clange("I", &N, &nrhs, h_X_gpu + s * ldb * nrhs, &ldb, work);
      //if(s == 9) {
        //magma_cprint(N, nrhs, h_X_gpu + s * ldb * nrhs, ldb);
      //}

      if(err > error) {
        error = err;
        idx = s;
      }
    }
    //std::cout<<error<<"\t"<<idx<<std::endl;
    okay = (error < tol);
    return okay;
  }
}

#endif
