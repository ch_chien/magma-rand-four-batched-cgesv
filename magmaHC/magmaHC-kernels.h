#ifndef magmaHC_kernels_h
#define magmaHC_kernels_h
// ============================================================================
// Header file declaring all kernels
//
// Modifications
//    Chien  21-05-04:   Add kernel_cgesv_batched_four_rand_fuse_reg,
//                           kernel_cgesv_batched_four_rand_mulk,
//                           kernel_get_new_data_four_rand,
//                           CPU_four_rand,
//                           compute_residual
//
// ============================================================================
#include <stdio.h>
#include <stdlib.h>
#include <cstdio>
#include <iostream>
#include <iomanip>
#include <cstring>
#include <chrono>

// -- cuda --
#include <cuda.h>
#include <cuda_runtime.h>

// -- magma --
#include "flops.h"
#include "magma_v2.h"

extern "C" {
namespace magmaHCWrapper {

  // -- four randomized cgesv, fused kernel using registers --
  void kernel_cgesv_batched_four_rand_fuse_reg(
    magma_int_t N, magma_int_t batchCount, magma_queue_t my_queue,
    magmaFloatComplex** dA_array, magma_int_t ldda, magma_int_t** dipiv_array,
    magmaFloatComplex **dB_array, magma_int_t lddb,
    magma_int_t* dinfo_array
  );

  // -- four randomized cgesv, fused kernel using global memory --
  void kernel_cgesv_batched_four_rand_fuse_gm(
    magma_int_t N, magma_int_t batchCount, magma_queue_t my_queue,
    magmaFloatComplex** dA_array, magma_int_t ldda, magma_int_t** dipiv_array,
    magmaFloatComplex **dB_array, magma_int_t lddb,
    magma_int_t* dinfo_array
  );

  // -- four randomized cgesv, multiple kernels --
  void kernel_cgesv_batched_four_rand_mulk(
    magma_int_t N, magma_int_t batchCount, magma_queue_t my_queue,
    magmaFloatComplex** dA_array, magma_int_t ldda, magma_int_t** dipiv_array,
    magmaFloatComplex **dB_array, magma_int_t lddb,
    magma_int_t* dinfo_array
  );

  // -- four randomized cgesv, get new data used in multiple kernels --
  void kernel_get_new_data_four_rand(
    magma_int_t N, magma_int_t batchCount, magma_queue_t my_queue,
    magmaFloatComplex** dA_array, magma_int_t ldda,
    magmaFloatComplex **dB_array
  );

  // -- randomized 4 batched linear systems solver computed on CPU --
  void CPU_four_rand(
    magmaFloatComplex *h_X_cpu,
    magmaFloatComplex *h_A, magmaFloatComplex *h_B,
    magmaFloatComplex *h_one_mat, magmaFloatComplex *h_one_vec,
    magma_int_t batchCount, magma_int_t N, magma_int_t nrhs,
    magma_int_t *ipiv, magma_int_t lda, magma_int_t ldb
  );

  // -- compare GPU's results with CPU's --
  bool compute_residual(
    magmaFloatComplex *h_X_cpu, magmaFloatComplex *h_X_gpu,
    magma_int_t N, magma_int_t nrhs, magma_int_t batchCount,
    magma_int_t ldb, float *work
  );
}
}

#endif
